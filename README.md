# Sample starter JSF 2 application with CDI that works on jetty

This is an 'empty' starter application to develop a JSF2 application with CDI that works on jetty.

This application does not actually works on glassfish 3.1 (nothing found on the internet yet to solve the issue...)

This is probably a dependency problem.

## Going further

Explore the branches on this repo for JSF2 examples based on this starting application.